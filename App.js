import React from 'react';
import {KeyboardAvoidingView, StyleSheet, Text, View, Platform} from 'react-native';
import { MenuProvider } from 'react-native-popup-menu';
import Routing from "./components/Routing/Routing";
import {Provider} from "react-redux";
import store from "./store";

export default function App() {
  return <Provider store={store}>
    <MenuProvider>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{
          backgroundColor: '#292e32',
          height: '100%'
        }}
      >
        <Routing />
      </KeyboardAvoidingView>
    </MenuProvider>
  </Provider>
}