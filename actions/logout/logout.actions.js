import {LOGOUT} from "./logout.constants";
import simpleAction from "../../utilities/simpleAction";

export const [
  logout
] = [
  simpleAction(LOGOUT)
]