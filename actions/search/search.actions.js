import {CLEAR_SEARCH, SEARCH} from "./search.constants";
import simpleAction from "../../utilities/simpleAction";

export const [
  search,
  clearSearch
] = [
  simpleAction(SEARCH),
  simpleAction(CLEAR_SEARCH)
]