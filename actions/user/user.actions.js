import {
  AVATAR_CHANGED,
  PERSONAL_DATA_FORM_SUBMIT,
  USERNAME_CHANGED,
  LOAD_USER
} from "./user.constants";
import simpleAction from "../../utilities/simpleAction";

export const [
  personalDataFormSubmit,
  avatarChanged,
  usernameChanged,
  loadUser
] = [
  simpleAction(PERSONAL_DATA_FORM_SUBMIT),
  simpleAction(AVATAR_CHANGED),
  simpleAction(USERNAME_CHANGED),
  simpleAction(LOAD_USER)
];