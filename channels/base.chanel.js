import io from "socket.io-client";
import {API_URL} from 'react-native-dotenv';
import {AsyncStorage} from "react-native";

export const socket = io.connect(API_URL);

class BaseChanel {
  constructor() {
    AsyncStorage.getItem('session').then(res => {
      socket.emit('Authorization', JSON.parse(res));
    })
  }
  subscribe(cb, event) {
    socket.on(event, cb);
  }
  unsubscribe(cb, event) {
    socket.removeListener(event, cb);
  }
}

export default new BaseChanel();