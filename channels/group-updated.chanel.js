import { eventChannel } from 'redux-saga';
import baseChanel from "./base.chanel";
import {GROUP_UPDATED} from "../actions/chats/chats.constants";

export default function createGroupUpdatedChanel() {
  const subscribe = emitter => {
    const cb = data => emitter(data);
    baseChanel.subscribe(cb, GROUP_UPDATED);
    return () => baseChanel.unsubscribe(cb, GROUP_UPDATED);
  };
  return eventChannel(subscribe);
}