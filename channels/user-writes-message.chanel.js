import { eventChannel } from 'redux-saga';
import {USER_WRITES_MESSAGE} from "../actions/chats/chats.constants";
import baseChanel from "./base.chanel";

export default function createWritesMessageChanel() {
  const subscribe = emitter => {
    const cb = data => emitter(data);
    baseChanel.subscribe(cb, USER_WRITES_MESSAGE);
    return () => baseChanel.unsubscribe(cb, USER_WRITES_MESSAGE);
  };
  return eventChannel(subscribe);
}