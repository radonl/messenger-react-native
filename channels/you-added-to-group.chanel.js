import { eventChannel } from 'redux-saga';
import {YOU_ADDED_TO_GROUP} from "../actions/chats/chats.constants";
import baseChanel from "./base.chanel";

export default function createYouAddedToGroupChanel() {
  const subscribe = emitter => {
    const cb = data => emitter(data);
    baseChanel.subscribe(cb, YOU_ADDED_TO_GROUP);
    return () => baseChanel.unsubscribe(cb, YOU_ADDED_TO_GROUP);
  };
  return eventChannel(subscribe);
}