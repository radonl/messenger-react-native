import {Platform, StyleSheet, View} from "react-native";
import {Icon} from "react-native-elements";
import React from "react";
import {BaseButton} from "../BaseButton/BaseButton";

const BackButton = ({handleClick}) => {
  return <BaseButton handleClick={handleClick}>
    <View style={styles.next}>
      <Icon
        color='white'
        name='arrow-left'
        type='font-awesome'
      />
    </View>
  </BaseButton>
}

const page = StyleSheet.create({
  next: {
    backgroundColor: "#393e42",
    width: 70,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    ...Platform.select({
      web: {
        cursor: 'pointer'
      }
    })
  }
});

const styles = StyleSheet.compose(page);

export default BackButton;