import React, { Component } from 'react';
import { Platform, StyleSheet, Text, TouchableHighlight, TouchableOpacity, TouchableNativeFeedback, TouchableWithoutFeedback, View } from 'react-native';


export const BaseButton = ({handleClick, children, styles={}}) => {
  return <View style={styles}>
    <TouchableOpacity onPress={handleClick} activeOpacity={0.6}>
      {children}
    </TouchableOpacity>
  </View>
}