import {Platform, StyleSheet, View} from "react-native";
import {Icon} from "react-native-elements";
import React from "react";
import {BaseButton} from "../BaseButton/BaseButton";

const NextButton = ({handleClick, mainContainerStyles}) => {
  return <BaseButton handleClick={handleClick} styles={mainContainerStyles}>
    <View style={styles.next}>
      <Icon
        color='white'
        name='arrow-right'
        type='font-awesome'
      />
    </View>
  </BaseButton>
}

const page = StyleSheet.create({
  next: {
    backgroundColor: "#36393d",
    width: 70,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    ...Platform.select({
      web: {
        cursor: 'pointer'
      }
    })
  }
});

const styles = StyleSheet.compose(page);

export default NextButton;