import React from "react";
import {Platform, StyleSheet, Text, View} from "react-native";
import BackButton from "../Buttons/BackButton/BackButton";

const Header = ({title, history, backUrl = '', actions, marginLeft = 0}) => {
  return <View style={{
    ...page.header,
    padding: backUrl !== '' ? 0: 20,
  }}>
    {backUrl !== '' && <BackButton handleClick={() => history.push(backUrl)}/>}
    <View style={{
      ...page.title,
      marginLeft
    }}>
      <Text style={page.text}>{title}</Text>
    </View>
    {!!actions && <View>
      {actions}
    </View>}
  </View>
};

const page = {
  header: {
    paddingTop: 30,
    padding: 20,
    width: '100%',
    backgroundColor: '#393e42',
    flexDirection: 'row',
    alignItems: 'center',
    ...Platform.select({
      web: {
        cursor: 'pointer'
      }
    })
  },
  title: {
    alignItems: 'center',
    flexGrow: 1
  },
  text: {
    fontSize: 25,
    color: '#ffffff'
  },
};

export default Header;