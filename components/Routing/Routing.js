import {Route, Router, Switch} from '../../utilities/routing'
import React, {useEffect, useState} from "react";
import AuthorizationService from "../../services/authorization.service"
import LoginScreen from "../../screens/Login/LoginScreen";
import MainScreen from "../../screens/Main/MainScreen";
import {StyleSheet, Text, View, Platform} from 'react-native';
import PhoneVerificationScreen from "../../screens/PhoneVerification/PhoneVerificationScreen";
import {useDispatch, useSelector} from "react-redux";
import MessagesScreen from "../../screens/Messages/MessagesScreen";
import {loadUser} from "../../actions/user/user.actions";
import SelectUsersForNewGroupScreen from "../../screens/SelectUsersForNewGroup/SelectUsersForNewGroupScreen";
import SelectNewGroupNameScreen from "../../screens/SelectNewGroupName/SelectNewGroupNameScreen";
import EditGroupScreen from "../../screens/EditGroup/EditGroupScreen";
import AddMemberToGroupScreen from "../../screens/AddMemberToGroup/AddMemberToGroupScreen";
import SettingsScreen from "../../screens/Settings/SettingsScreen";

const Routing = () => {
  const [authorizationChecked, setAuthorizationChecked] = useState(false);
  const [isAuthorized, setIsAuthorized] = useState(false);
  const login = useSelector(state => state.login);
  const dispatch = useDispatch();
  useEffect(() => {
    AuthorizationService.isAuthorized().then(authorized => {
      setIsAuthorized(authorized);
      setAuthorizationChecked(true);
    })
  }, [login]);
  useEffect(() => {
    dispatch(loadUser());
  }, [])
  return authorizationChecked ? <Router>
    <Route render={({location}) => {
      return  <Switch location={location}>
        {isAuthorized && <Route exact path={'/'} component={MainScreen}/>}
        {isAuthorized && <Route exact path={'/messages'} component={MessagesScreen}/>}
        {isAuthorized && <Route exact path={'/createGroup'} component={SelectUsersForNewGroupScreen}/>}
        {isAuthorized && <Route exact path={'/createGroupName'} component={SelectNewGroupNameScreen}/>}
        {isAuthorized && <Route exact path={'/editGroup'} component={EditGroupScreen}/>}
        {isAuthorized && <Route exact path={'/settings'} component={SettingsScreen}/>}
        {isAuthorized && <Route exact path={'/addMemberToGroup'} component={AddMemberToGroupScreen} /> }
        {!isAuthorized && <Route exact path={'/'} component={LoginScreen}/>}
        {!isAuthorized &&  <Route exact path={'/verify'} component={PhoneVerificationScreen}/>}
      </Switch>;
    }}/>
  </Router> : <View>
    <Text>Loading...</Text>
  </View>;
}

export default Routing;