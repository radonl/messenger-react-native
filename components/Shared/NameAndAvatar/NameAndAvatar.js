import {Image, ScrollView, Text, TextInput, View} from "react-native";
import {BaseButton} from "../../Buttons/BaseButton/BaseButton";
import {Icon} from "react-native-elements";
import React, {useState} from "react";
import * as ImagePicker from "expo-image-picker";

const NameAndAvatar = ({avatar, avatarChanged, name, nameChanged, namePlaceholder='Enter new group name'}) => {
  const pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        base64: true,
        quality: 1,
        aspect: [1,1]
      });
      if (!result.cancelled) {
        avatarChanged(result.base64 ? `data:image/png;base64,${result.base64}`: result.uri);
      }
    } catch (e) {
      alert(e);
    }
  }
  return <View style={styles.baseInfoContainer}>
    <BaseButton styles={{
      ...styles.addAvatar,
      padding: avatar === '' ? 20 : 0
    }} handleClick={pickImage}>
      <View>
        {avatar === '' ? <Icon
          color='white'
          name='add-a-photo'
          type='material'
          size={25}
        /> : <Image style={styles.avatarImg} source={{uri: avatar}} />}
      </View>
    </BaseButton>
    <View style={styles.groupNameContainer}>
      <TextInput
        style={styles.groupName}
        placeholder={namePlaceholder}
        placeholderTextColor='#a5a5a5'
        defaultValue={name}
        onChangeText={text => nameChanged(text)}
      />
    </View>
  </View>
};

const styles = {
  baseInfoContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 20,
    backgroundColor: '#303337'
  },
  addAvatar: {
    backgroundColor: '#404347',
    padding: 20,
    borderRadius: 500
  },
  avatarImg: {
    height: 65,
    width: 65,
    borderRadius: 45
  },
  groupNameContainer: {
    flexGrow: 1,
    paddingLeft: 20,
    justifyContent: 'center'
  },
  groupName: {
    borderBottomWidth: 1,
    borderBottomColor: '#606367',
    fontSize: 20,
    color: 'white'
  },
};

export default NameAndAvatar;