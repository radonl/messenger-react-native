import {StyleSheet, Text, View, Image} from 'react-native';
import React from "react";
import {useDispatch} from "react-redux";
import {BaseButton} from "../Buttons/BaseButton/BaseButton";
import {selectChat} from "../../actions/chats/chats.actions";
import {Icon} from "react-native-elements";

const User = ({chat, handleClick, removable = false, handleRemove, showAdd = false, handleAdd}) => {
  return <BaseButton handleClick={() => {
    if(handleClick) {
      handleClick();
    }
  }}>
    <View style={{
      ...styles.container,
      ...chat.selected ? {
        backgroundColor: '#afaca8',
        borderBottomColor: '#a6a19d'
      }: {}
    }}>
      <View style={styles.avatarContainer}>
        <Image
          style={styles.avatar}
          source={{uri: chat.avatarPath}}
        />
      </View>
      <View style={styles.mainContainer}>
        <View>
          <Text style={{
            ...styles.name,
            ...chat.selected ? {
              color: '#333'
            }: {}
          }}>{chat.name}</Text>
        </View>
        <View>
          <Text style={styles.lastMessage}>
            {chat.lastMessage}
          </Text>
        </View>
      </View>
      <View style={styles.timeContainer}>
        <Text style={styles.time}>
          {chat.lastMessageTime}
        </Text>
      </View>
      {chat.selected && <View style={styles.checkedContainer}>
        <Icon
          color='white'
          name='check'
          size={30}
          type='material'
        />
      </View>}
      {removable && <BaseButton handleClick={handleRemove} styles={styles.checkedContainer}>
        <View>
          <Icon
            color='#cc4545'
            name='clear'
            size={30}
            type='material'
          />
        </View>
      </BaseButton>}
      {showAdd && <BaseButton handleClick={handleAdd} styles={styles.checkedContainer}>
        <View>
          <Icon
            color='#008888'
            name='add'
            size={30}
            type='material'
          />
        </View>
      </BaseButton>}
    </View>
  </BaseButton>
};

const styles = {
  container: {
    width: '100%',
    height: 80,
    flexDirection: 'row',
    backgroundColor: '#303337',
    borderBottomWidth: 2,
    borderBottomColor: '#393e42'
  },
  avatarContainer: {
    width: 80,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mainContainer: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  timeContainer: {
    width: 70,
    justifyContent: 'center'
  },
  avatar: {
    borderRadius: 30,
    width: 50,
    height: 50
  },
  name: {
    color: 'white',
    fontSize: 18,
    fontWeight: '600'
  },
  time: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500'
  },
  lastMessage: {
    color: '#a5a5a5',
    fontSize: 15,
    fontWeight: '500'
  },
  checkedContainer: {
    justifyContent: 'center',
    width: 75
  }
};

export default User;