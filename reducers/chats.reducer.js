import {
  CHAT_CREATED,
  CHATS_LOADED,
  CLEAR_USER_WRITES_MESSAGE, GROUP_AVATAR_UPDATED,
  GROUP_CREATED,
  GROUP_NAME_UPDATED,
  GROUP_REMOVED, GROUP_UPDATED,
  INCOMING_MESSAGE,
  INIT_MESSAGE_SENT,
  PREPARE_TO_CREATE_CHAT, PREPARE_TO_CREATE_GROUP,
  SELECT_CHAT,
  USER_ADDED_TO_GROUP,
  USER_REMOVED_FROM_GROUP,
  USER_WRITES_MESSAGE, YOU_ADDED_TO_GROUP
} from "../actions/chats/chats.constants";
import moment from "moment";

const transformChat = (chat, rootState) => {
  if (chat.isPrivate) {
    const chatMember = chat.members.find(member => member.id !== rootState.user.id);
    chat.name = chatMember.displayedName || chatMember.username || chatMember.phone;
    chat.avatarPath = chatMember.avatarPath;
    chat.partnerId = chatMember.id;
  } else {
    chat.adminAccess = rootState.user.id === chat.ownerId;
  }
  chat.members = chat.members.map(member => memberTransform(member));
  chat.lastMessage = (chat.messages && chat.messages.length === 1) ? chat.messages[0].message : '';
  chat.lastMessageTime = (chat.messages && chat.messages.length === 1) ? moment(chat.messages[0].updatedAt).format('LT') : '';
  return chat;
}

const memberTransform = member => {
  member.name = member.displayedName || member.username || member.phone
  return member;
}

export const chatsReducer = (state, action, rootState) => {
  switch (action.type) {
    case CHATS_LOADED:
      state.chats = action.payload.data.map(chat => transformChat(chat, rootState));
      break;
    case INIT_MESSAGE_SENT:
    case SELECT_CHAT:
      state.chats = state.chats.map(chat => {
        chat.selected = chat.id === action.payload.id;
        return chat;
      });
      state.selectedChat = state.chats.find(chat => action.payload.id === chat.id);
      break;
    case USER_WRITES_MESSAGE:
      if (action.payload.chatId === state.selectedChat.id && action.payload.userId !== rootState.user.id) {
        const user = rootState.users.find(user => user.id === action.payload.userId);
        if (user) {
          state.userWritingMessage = `${user.name} is writing message...`
        }
      }
      break;
    case CLEAR_USER_WRITES_MESSAGE:
      state.userWritingMessage = '';
      break;
    case INCOMING_MESSAGE:
      state.chats = state.chats.map(chat => {
        if (chat.id === action.payload.chatId) {
          chat.lastMessage = action.payload.message;
        }
        return chat;
      })
      break;
    case PREPARE_TO_CREATE_CHAT:
      state.isPrepareToCreateChat = true;
      state.newChatPartner = action.payload;
      state.selectedChat = {};
      break;
    case USER_REMOVED_FROM_GROUP:
      state.chats = state.chats.map(chat => {
        if (chat.id === action.payload.chatId) {
          chat.members = chat.members.filter(member => member.id !== action.payload.userId);
        }
        return chat;
      })
      break;
    case USER_ADDED_TO_GROUP:
      state.chats = state.chats.map(chat => {
        if (chat.id === action.payload.chatId) {
          chat.members.push(rootState.users.find(user => user.id === action.payload.userId))
        }
        return chat;
      });
      break;
    case GROUP_REMOVED:
      state.chats = state.chats.filter(chat => chat.id !== action.payload);
      if (state.selectedChat.id === action.payload) {
        state.selectedChat = {}
      }
      break;
    case GROUP_UPDATED:
      state.chats = state.chats.map(chat => {
        if (chat.id === action.payload.id) {
          chat.name = action.payload.name;
          chat.avatarPath = action.payload.avatarPath;
          chat.members = action.payload.members.map(member => memberTransform(member));
        }
        return chat;
      });
      break;
    case GROUP_NAME_UPDATED:
    case GROUP_AVATAR_UPDATED:
      state.chats = state.chats.map(chat => {
        if (chat.id === action.payload.data.id) {
          chat.name = action.payload.data.name;
          chat.avatarPath = action.payload.data.avatarPath;
          chat.members = chat.members.map(member => memberTransform(member));
        }
        return chat;
      });
      break;
    case YOU_ADDED_TO_GROUP:
      if (!state.chats.find(chat => chat.id === action.payload.id)) {
        state.chats.push(transformChat(action.payload, rootState));
      }
      break;
    case PREPARE_TO_CREATE_GROUP:
      state.newGroup.selectedUsers = action.payload;
      break;
    default:
      break;
  }
  return {
    ...state
  }
}

export const chatsDefaultState = {
  chats: [],
  selectedChat: {},
  isPrepareToCreateChat: false,
  newChatPartner: {},
  userWritingMessage: '',
  clearWritingMessageTimeout: null,
  newGroup: {
    selectedUsers: []
  }
};