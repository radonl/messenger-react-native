import {loginDefaultState, loginReducer} from "./login.reducer";
import {chatsDefaultState, chatsReducer} from "./chats.reducer";
import {usersDefaultState, usersReducer} from "./users.reducer";
import {messagesDefaultState, messagesReducer} from "./messages.reducer";
import {userDefaultState, userReducer} from "./user.reducer";
import {searchDefaultState, searchReducer} from "./search.reducer";

const rootState = {
  login: loginDefaultState,
  messages: messagesDefaultState,
  chats: chatsDefaultState,
  users: usersDefaultState,
  user: userDefaultState,
  search: searchDefaultState
};

const rootReducer = (state = rootState, action) => ({
  login: loginReducer(state.login, action),
  chats: chatsReducer(state.chats, action, state),
  users: usersReducer(state.users, action, state),
  messages: messagesReducer(state.messages, action, state),
  user: userReducer(state.user, action, state),
  search: searchReducer(state.search, action, state)
});

export default rootReducer;