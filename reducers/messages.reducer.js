import {
  GROUP_CHAT_CLEARED,
  INCOMING_MESSAGE,
  MESSAGE_SENT,
  MESSAGES_LOADED,
  MESSAGES_LOADING_FAILED
} from "../actions/chats/chats.constants";
import moment from "moment";

export const messagesReducer = (state, action, rootState) => {
  switch (action.type) {
    case MESSAGES_LOADED:
      state.messagesLoadingFailed = false
      state.messages[rootState.chats.selectedChat.id] = action.payload.data.map(message => {
        message.displayedTime = moment(message.updatedAt).format('LTS');
        return message;
      });
      return {
        ...state
      };
    case MESSAGES_LOADING_FAILED:
      return {
        ...state,
        messagesLoadingFailed: true
      };
    case INCOMING_MESSAGE:
      if(rootState.chats.selectedChat.id === action.payload.chatId && state.messages[rootState.chats.selectedChat.id]) {
        state.messages[rootState.chats.selectedChat.id].push({
          ...action.payload,
          user: rootState.users.find(user => user.id === action.payload.senderId),
          displayedTime: moment(action.payload.updatedAt).format('LTS')
        });
      }
      return {
        ...state
      };
    case MESSAGE_SENT: {
      return {
        ...state
      };
    }
    case GROUP_CHAT_CLEARED:
      state.messages[action.payload] = [];
      return {
        ...state
      }
    default: {
      return state;
    }
  }
}

export const messagesDefaultState = {
  messages: {},
  messagesLoadingFailed: false,
  userWritingMessage: false,
};