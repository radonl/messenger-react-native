import {
  AVATAR_CHANGED_SUCCESS,
  PERSONAL_DATA_SAVING_SUCCESS, USER_LOADED,
  USERNAME_CHANGED_SUCCESS
} from "../actions/user/user.constants";
import {AsyncStorage} from "react-native";

export const userReducer = (state, action) => {
  switch (action.type) {
    case AVATAR_CHANGED_SUCCESS:
    case PERSONAL_DATA_SAVING_SUCCESS:
    case USERNAME_CHANGED_SUCCESS:
      AsyncStorage.setItem('user', JSON.stringify(action.payload.data)).then();
      return {
        ...action.payload.data
      }
    case USER_LOADED:
      return JSON.parse(action.payload);
    default: {
      return state;
    }
  }
};

export const userDefaultState = {
};