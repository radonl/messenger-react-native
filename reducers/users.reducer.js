import {
  CHATS_LOADED,
  CLEAR_USER_WRITES_MESSAGE,
  SELECT_CHAT,
  USER_WRITES_MESSAGE, YOU_ADDED_TO_GROUP
} from "../actions/chats/chats.constants";
import {
  AVATAR_CHANGED_SUCCESS,
  PERSONAL_DATA_SAVING_SUCCESS,
  USERNAME_CHANGED_SUCCESS
} from "../actions/user/user.constants";

export const usersReducer = (state, action, rootState) => {
  switch (action.type) {
    case AVATAR_CHANGED_SUCCESS:
    case PERSONAL_DATA_SAVING_SUCCESS:
    case USERNAME_CHANGED_SUCCESS:
      return state.map(user => {
        if (user.id === action.payload.data.id) {
          user = action.payload.data;
          user.name = user.displayedName || user.username || user.phone;
        }
        return user;
      })
    case CHATS_LOADED:
      return [
        ...state,
        ...action.payload.data.reduce((res, chat) => {
          return [...res, ...chat.members.filter(user => !state.find(savedUser => user.id === savedUser.id))]
        }, []).map(user => {
          user.name = user.displayedName || user.username || user.phone;
          return user;
        })
      ].filter((user, index, self) => index === self.findIndex(iUser => iUser.id === user.id));
    case YOU_ADDED_TO_GROUP:
      return [
        ...state,
        ...action.payload.members.reduce((res, member) => {
          if(!state.find(user => user.id === member.id)) {
            res.push(member);
          }
          return res;
        }, [])
      ]
    default: {
      return state;
    }
  }
}

export const usersDefaultState = [];