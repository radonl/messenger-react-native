import {put, take, call, fork} from "@redux-saga/core/effects";
import createMessagesChanel from "../channels/messages.chanel";
import createWritesMessageChanel from "../channels/user-writes-message.chanel";
import {
  CLEAR_USER_WRITES_MESSAGE,
  GROUP_UPDATED,
  INCOMING_MESSAGE,
  USER_WRITES_MESSAGE,
  YOU_ADDED_TO_GROUP
} from "../actions/chats/chats.constants";
import createYouAddedToGroupChanel from "../channels/you-added-to-group.chanel";
import createGroupUpdatedChanel from "../channels/group-updated.chanel";

const incomingMessage = function* () {
  const channel = yield call(createMessagesChanel);
  while (true) {
    const result = yield take(channel);
    yield put({
      type: INCOMING_MESSAGE,
      payload: result
    })
  }
};

const userWritesMessage = function* () {
  const channel = yield call(createWritesMessageChanel);
  while (true) {
    const result = yield take(channel);
    yield put({
      type: USER_WRITES_MESSAGE,
      payload: result
    })
    const action = yield (new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          type: CLEAR_USER_WRITES_MESSAGE
        })
      }, 1500);
    }));
    yield put(action);
  }
};

const youAddedToGroup = function* () {
  const channel = yield call(createYouAddedToGroupChanel);
  while (true) {
    const result = yield take(channel);
    yield put({
      type: YOU_ADDED_TO_GROUP,
      payload: result
    })
  }
}

const groupUpdated = function* () {
  const chanel = yield call(createGroupUpdatedChanel);
  while (true) {
    const result = yield take(chanel);
    yield put({
      type: GROUP_UPDATED,
      payload: result
    });
  }
}

export default [
  fork(incomingMessage),
  fork(userWritesMessage),
  fork(youAddedToGroup),
  fork(groupUpdated)
];