import {takeEvery} from "@redux-saga/core/effects";

import {
  LOAD_CHATS,
  CHATS_LOADED,
  CHATS_LOADING_FAILED,
  CREATE_CHAT,
  CHAT_CREATED,
  CHAT_CREATION_FAILED,
  CREATE_GROUP,
  GROUP_CREATED,
  GROUP_CREATION_FAILED,
  REMOVE_USER_FROM_GROUP,
  USER_REMOVED_FROM_GROUP,
  USER_REMOVING_FROM_GROUP_FAILED,
  ADD_USER_TO_GROUP,
  USER_ADDED_TO_GROUP,
  USER_ADDING_TO_GROUP_FAILED,
  GROUP_REMOVE,
  GROUP_REMOVED,
  GROUP_REMOVING_FAILED,
  CLEAR_GROUP_CHAT,
  GROUP_CHAT_CLEARED,
  GROUP_CHAT_CLEARING_FAILED,
  UPDATE_GROUP_NAME,
  GROUP_NAME_UPDATED,
  GROUP_NAME_UPDATING_FAILED,
  UPDATE_GROUP_AVATAR,
  GROUP_AVATAR_UPDATED, GROUP_AVATAR_UPDATING_FAILED
} from "../actions/chats/chats.constants";
import ChatsService from "../services/chats.service";
import simpleSaga from "../utilities/simpleSaga";

export const chatsSaga = function* () {
  yield takeEvery(LOAD_CHATS, simpleSaga(ChatsService.loadChats, CHATS_LOADED, CHATS_LOADING_FAILED));
  yield takeEvery(CREATE_CHAT, simpleSaga(ChatsService.createChat, CHAT_CREATED, CHAT_CREATION_FAILED));
  yield takeEvery(CREATE_GROUP, simpleSaga(ChatsService.createGroup, GROUP_CREATED, GROUP_CREATION_FAILED));
  yield takeEvery(REMOVE_USER_FROM_GROUP, simpleSaga(
    ChatsService.removeUserFromGroup,
    USER_REMOVED_FROM_GROUP,
    USER_REMOVING_FROM_GROUP_FAILED
  ));
  yield takeEvery(ADD_USER_TO_GROUP, simpleSaga(
    ChatsService.addUserToGroup,
    USER_ADDED_TO_GROUP,
    USER_ADDING_TO_GROUP_FAILED
  ));
  yield takeEvery(GROUP_REMOVE, simpleSaga(
    ChatsService.removeGroup,
    GROUP_REMOVED,
    GROUP_REMOVING_FAILED
  ));
  yield takeEvery(CLEAR_GROUP_CHAT, simpleSaga(
    ChatsService.clearGroup,
    GROUP_CHAT_CLEARED,
    GROUP_CHAT_CLEARING_FAILED
  ));
  yield takeEvery(UPDATE_GROUP_NAME, simpleSaga(
    ChatsService.updateGroup,
    GROUP_NAME_UPDATED,
    GROUP_NAME_UPDATING_FAILED
  ));
  yield takeEvery(UPDATE_GROUP_AVATAR, simpleSaga(
    ChatsService.updateGroup,
    GROUP_AVATAR_UPDATED,
    GROUP_AVATAR_UPDATING_FAILED
  ))
};