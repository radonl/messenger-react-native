import {takeEvery} from "@redux-saga/core/effects";

import AuthorizationService from '../services/authorization.service';
import {
  AUTHORIZATION,
  AUTHORIZATION_FAILED,
  AUTHORIZATION_SUCCESSFULLY,
  CODE_SENDING_FAILED,
  CODE_SENT, SEND_CODE
} from "../actions/login/login.constants";
import simpleSaga from "../utilities/simpleSaga";

export const loginSaga = function* () {
  yield takeEvery(SEND_CODE, simpleSaga(AuthorizationService.sendCode, CODE_SENT, CODE_SENDING_FAILED));
  yield takeEvery(AUTHORIZATION, simpleSaga(AuthorizationService.login, AUTHORIZATION_SUCCESSFULLY, AUTHORIZATION_FAILED));
};