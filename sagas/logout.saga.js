import {takeEvery} from "@redux-saga/core/effects";
import AuthorizationService from "../services/authorization.service";
import {LOGOUT, LOGOUT_FAILED, LOGOUT_SUCCESS} from "../actions/logout/logout.constants";
import simpleSaga from "../utilities/simpleSaga";

export const logoutSaga = function* () {
  yield takeEvery(LOGOUT, simpleSaga(AuthorizationService.logout, LOGOUT_SUCCESS, LOGOUT_FAILED));
};