import {takeEvery} from "@redux-saga/core/effects";
import {
  CHAT_CREATED, INIT_MESSAGE_SENT,
  MESSAGE_SENDING_FAILED,
  MESSAGE_SENT,
  MESSAGES_LOADED,
  MESSAGES_LOADING_FAILED, NO_REACTION,
  SELECT_CHAT,
  SEND_MESSAGE,
  WRITING_MESSAGE,
  SEND_FILE,
  FILE_SENT,
  FILE_SENDING_FAILED
} from "../actions/chats/chats.constants";
import ChatsService from '../services/chats.service';
import simpleSaga from "../utilities/simpleSaga";

export const messagesSaga = function* () {
  yield takeEvery(SELECT_CHAT, simpleSaga(ChatsService.loadMessages, MESSAGES_LOADED, MESSAGES_LOADING_FAILED));
  yield takeEvery(SEND_MESSAGE, simpleSaga(ChatsService.sendMessage, MESSAGE_SENT, MESSAGE_SENDING_FAILED));
  yield takeEvery(SEND_FILE, simpleSaga(ChatsService.sendFile, FILE_SENT, FILE_SENDING_FAILED));
  yield takeEvery(WRITING_MESSAGE, simpleSaga(ChatsService.writingMessage, NO_REACTION, NO_REACTION));
  yield takeEvery(CHAT_CREATED, simpleSaga(ChatsService.sendInitMessage,  INIT_MESSAGE_SENT, MESSAGE_SENDING_FAILED));
  yield takeEvery(INIT_MESSAGE_SENT, simpleSaga(ChatsService.loadMessages, MESSAGES_LOADED, MESSAGES_LOADING_FAILED));
};