import {takeEvery} from "@redux-saga/core/effects";
import chatsService from "../services/chats.service";
import {SEARCH, SEARCH_FAILED, SEARCH_RESULTS_LOADED} from "../actions/search/search.constants";
import simpleSaga from "../utilities/simpleSaga";

export const searchSaga = function* () {
  yield takeEvery(SEARCH, simpleSaga(chatsService.search, SEARCH_RESULTS_LOADED, SEARCH_FAILED))
}