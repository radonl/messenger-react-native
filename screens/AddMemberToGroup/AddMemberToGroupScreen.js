import React, {useEffect, useState} from "react";
import {ScrollView, StyleSheet, Text, View} from "react-native";
import Header from "../../components/Header/Header";
import {useDispatch, useSelector} from "react-redux";
import User from "../../components/User/User";
import {addUserToGroup} from "../../actions/chats/chats.actions";

const AddMemberToGroupScreen = ({history}) => {
  const {selectedChat} = useSelector(state => state.chats);
  const [availableUsers, setAvailableUsers] = useState([]);
  const users = useSelector(state => state.users);
  const dispatch = useDispatch();
  useEffect(() => {
    setAvailableUsers(users.filter(user => !selectedChat.members.find(member => member.id === user.id)));
  }, [selectedChat.members, users])
  return <View>
    <Header title={`Add member to "${selectedChat.name}"`} history={history} backUrl={'editGroup'} marginLeft={-50}/>
    <ScrollView>
      {availableUsers && availableUsers.map((user, index) => {
        return <User key={`${user.id}.${index}`} chat={user} showAdd={true} handleAdd={() => {
          dispatch(addUserToGroup({
            userId: user.id,
            chatId: selectedChat.id
          }))
        }} />
      })}
    </ScrollView>
  </View>
};


const page = StyleSheet.create({

});

const styles = StyleSheet.compose(page);


export default AddMemberToGroupScreen;