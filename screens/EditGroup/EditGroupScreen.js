import {StyleSheet, Text, View, TextInput, Platform, ScrollView} from 'react-native';
import React, {useEffect, useState} from "react";
import Header from "../../components/Header/Header";
import {useDispatch, useSelector} from "react-redux";
import User from "../../components/User/User";
import NameAndAvatar from "../../components/Shared/NameAndAvatar/NameAndAvatar";
import {createGroup, removeUserFromGroup, updateGroupAvatar, updateGroupName} from "../../actions/chats/chats.actions";
import NextButton from "../../components/Buttons/NextButton/NextButton";
import SaveButton from "../../components/Buttons/SaveButton/SaveButton";
import {BaseButton} from "../../components/Buttons/BaseButton/BaseButton";
import {Icon} from "react-native-elements";

const EditGroupScreen = ({history}) => {
  const dispatch = useDispatch();
  const {selectedChat} = useSelector(state => state.chats);
  const user = useSelector(state => state.user);
  const [name, setName] = useState('');
  const [avatar, setAvatar] = useState('');
  useEffect(() => {
    setName(selectedChat.name);
    setAvatar(selectedChat.avatarPath);
  }, [selectedChat])
  return <View style={styles.container}>
    <Header history={history} backUrl={'messages'} title={`Edit "${selectedChat.name}"`} marginLeft={-50}/>
    <ScrollView>
      <NameAndAvatar
        avatar={avatar}
        avatarChanged={avatar => setAvatar(avatar)}
        name={name}
        nameChanged={name => setName(name)}
      />
      <BaseButton styles={styles.addMemberButton} handleClick={() => history.push('addMemberToGroup')}>
        <View style={styles.addMemberContainer}>
            <Text style={styles.addMemberText}>Add member</Text>
            <Icon
              color='white'
              name='person-add'
              type='material'
            />
        </View>
      </BaseButton>
      <View style={styles.usersContainer}>
        {!!selectedChat.members && selectedChat.members.map((member, index) => <User
          key={`${member.id}.${index}`}
          chat={member}
          removable={user.id !== member.id}
          handleRemove={() => {
            dispatch(removeUserFromGroup({
              userId: member.id,
              chatId: selectedChat.id
            }))
          }}
        />)}
      </View>
    </ScrollView>
    {(name !== selectedChat.name || avatar !== selectedChat.avatarPath) && <SaveButton
      handleClick={() => {
        if(name !== selectedChat.name) {
          dispatch(updateGroupName({
            id: selectedChat.id,
            name: name
          }))
        }
        if(avatar !== selectedChat.avatarPath) {
          dispatch(updateGroupAvatar({
            id: selectedChat.id,
            ...avatar.length > 0 ? {
              avatar: {
                fileExtension: 'png',
                fileContent: avatar.indexOf('base64') ? avatar.substr(22) : avatar
              }
            } : {}
          }))
        }
      }}
      mainContainerStyles={{
        position: 'absolute',
        right: 10,
        bottom: 10,
      }}
    />}
  </View>
};

const page = StyleSheet.create({
  container: {
    backgroundColor: '#292e32',
    height: '100%'
  },
  usersContainer: {
    marginTop: 10
  },
  addMemberButton: {
    marginTop: 10,
    backgroundColor: '#393e42',
    padding: 20,
  },
  addMemberContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  addMemberText: {
    color: 'white',
    fontSize: 18,
  }
});

const styles = StyleSheet.compose(page);

export default EditGroupScreen;