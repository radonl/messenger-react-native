import React from "react";
import { StyleSheet, Text, View, TextInput, Platform} from 'react-native';
import { Icon } from 'react-native-elements';
import {useDispatch, useSelector} from "react-redux";
import {phoneChanged, sendCode} from "../../actions/login/login.actions";
import NextButton from "../../components/Buttons/NextButton/NextButton";
import Header from "../../components/Header/Header";

const LoginScreen =  ({history}) => {
  const dispatch = useDispatch();
  const {phone} = useSelector(state => state.login);
  const toVerify = () => {
    dispatch(sendCode({phone}));
    history.push('verify')
  };
  return <View style={composedPage.container}>
    <Header title={"Your phone"}/>
    <TextInput defaultValue={"Ukraine"} editable={false} style={composedPage.disabledInput}/>
    <View style={composedPage.phoneContainer}>
      <TextInput defaultValue={"+380"} editable={false} style={composedPage.countryCode}/>
      <TextInput
        placeholder='-- --- -- --'
        value={phone}
        style={composedPage.phoneInput}
        onChangeText={text => dispatch(phoneChanged(text))}
      />
    </View>
    <View style={composedPage.phoneContainer}>
      <Text style={composedPage.helpMessage}>Please confirm your country code and enter your phone number</Text>
    </View>
    <View style={composedPage.nextContainer}>
      <View style={composedPage.nextSubContainer}>
        <NextButton handleClick={toVerify}/>
      </View>
    </View>
  </View>;
}

const page = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292e32',
  },
  nextContainer: {
    flex: 1,
    flexGrow: 1,
    flexDirection: 'column-reverse',
  },
  nextSubContainer: {
    flexDirection: 'row-reverse',
    padding: 20
  },
  disabledInput: {
    marginTop: 35,
    margin: 20,
    fontSize: 22,
    borderBottomColor: '#494e52',
    borderBottomWidth: 1,
    color: '#a1a1a1',
    padding: 5
  },
  phoneContainer: {
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  countryCode: {
    fontSize: 22,
    borderBottomColor: '#494e52',
    borderBottomWidth: 1,
    padding: 5,
    color: '#a1a1a1',
    width: 65
  },
  phoneInput: {
    fontSize: 22,
    borderBottomColor: '#494e52',
    borderBottomWidth: 1,
    padding: 5,
    color: '#e5e5e5',
    width: 250,
    minWidth: 0
  },
  helpMessage: {
    fontSize: 15,
    marginTop: 10,
    color: '#8b9bab'
  },
});


const composedPage = StyleSheet.compose(page);

export default LoginScreen;