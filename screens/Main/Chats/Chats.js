import React, {useEffect} from "react";
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import Search from "./Search/Search";
import {useDispatch, useSelector} from "react-redux";
import User from "../../../components/User/User";
import {prepareToCreateChat, selectChat} from "../../../actions/chats/chats.actions";

const Chats = ({history}) => {
  const {chats} = useSelector(state => state.chats);
  const {displayResults, results, requestInProgress} = useSelector(state => state.search);
  const dispatch = useDispatch();
  return <View>
    <Search />
    {displayResults ? <View>
      {requestInProgress ? <Text>Loading...</Text>:<View>
        {results.length === 0 ? <Text>
          No matches found
        </Text> : <View>
          {results.map((chat, index) => <User chat={chat} key={`${chat.id}.${index}`} handleClick={() => {
            dispatch(prepareToCreateChat(chat));
            history.push('messages');
          }}/>)}
        </View>}
      </View>}
    </View> : <View>
      {chats.map((chat, index) => {
        return <User chat={chat} history={history} key={`${chat.name}.${index}`} handleClick={() => {
          dispatch(selectChat({...chat}));
          history.push('/messages');
        }}/>
      })}
    </View>}
  </View>
};

export default Chats;