import React, {useState} from "react";
import {StyleSheet, View} from "react-native";
import {SearchBar} from "react-native-elements";
import {useDispatch} from "react-redux";
import {clearSearch, search} from "../../../../actions/search/search.actions";

const Search = () => {
  const [searchString, setSearchString] = useState('');
  const dispatch = useDispatch();
  return <View>
    <SearchBar
      containerStyle={{
        borderBottomColor: 'transparent',
        borderTopColor: 'transparent',
      }}
      placeholder="Search"
      onChangeText={text => {
        setSearchString(text);
        if(text) {
          dispatch(search(text));
        } else {
          dispatch(clearSearch())
        }
      }}
      value={searchString}
    />
  </View>
};
const page = StyleSheet.create({

});

const styles = StyleSheet.compose(page);

export default Search;