import React, {useEffect} from "react";
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import Chats from "./Chats/Chats";
import {loadChats} from "../../actions/chats/chats.actions";
import {useDispatch, useSelector} from "react-redux";
import {BaseButton} from "../../components/Buttons/BaseButton/BaseButton";
import {Icon} from "react-native-elements";
import Header from "../../components/Header/Header";


const MainScreen = ({history}) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(loadChats())
  }, []);
  return <View style={styles.container}>
    <Header
      history={history}
      title={'Chats'}
      marginLeft={85}
      actions={
        <View style={styles.iconsContainer}>
          <BaseButton styles={styles.createGroup} handleClick={() => history.push('createGroup')}>
            <View>
              <Icon
                color='white'
                name='plus'
                type='font-awesome'
              />
            </View>
          </BaseButton>
          <BaseButton styles={styles.createGroup} handleClick={() => history.push('settings')}>
            <View>
              <Icon
                color='white'
                name='settings'
                type='material'
              />
            </View>
          </BaseButton>
        </View>
    }/>
    <ScrollView>
      <Chats history={history}/>
    </ScrollView>
  </View>
};

const page = StyleSheet.create({
  header: {
    backgroundColor: '#393e42',
    flexDirection: 'row',
    paddingBottom: 2
  },
  title: {
    alignItems: 'center',
    flexGrow: 1
  },
  text: {
    fontSize: 20,
    color: 'white',
    marginLeft: 50
  },
  createGroup: {
    width: 50,
    justifyContent: 'center'
  },
  container: {
    backgroundColor: '#393e42',
    height: '100%'
  },
  iconsContainer: {
    flexDirection: 'row',
    width: 85,
    justifyContent: 'space-between'
  }
});

const styles = StyleSheet.compose(page);

export default MainScreen;

// <View style={styles.header}>
//   <View style={styles.title}>
//     <Text style={styles.text}>Chats</Text>
//   </View>
//   <BaseButton styles={styles.createGroup} handleClick={() => history.push('createGroup')}>
//     <View>
//       <Icon
//         color='white'
//         name='plus'
//         type='font-awesome'
//       />
//     </View>
//   </BaseButton>
// </View>