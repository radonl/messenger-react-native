import React from "react";
import {Image, StyleSheet, Text, View} from "react-native";
import {useSelector} from "react-redux";

const Message = ({message}) => {
  const users = useSelector(state => state.users);
  const sender = users.find(user => user.id === message.senderId);
  const user = useSelector(state => state.user);
  return sender ? <View style={sender.id === user.id ? styles.containerReverse : styles.container}>
    <View style={styles.avatarContainer}>
      <Image
        source={{uri: sender.avatarPath}}
        style={styles.avatar}
      />
    </View>
    <View style={sender.id === user.id ? styles.mainContainerReverse : styles.mainContainer}>
      <View style={styles.nameAndTextContainer}>
        <Text style={styles.name}>{sender.name}</Text>
        <Text style={styles.message}>{message.message}</Text>
        {message.file && message.file.path && <Image
          source={{uri: message.file.path}}
          height={100}
          width={100}
          style={{
            height: 100,
            width: 100
          }}
        />}
      </View>
      <View style={styles.timeContainer}>
        <Text style={styles.time}>{message.displayedTime}</Text>
      </View>
    </View>
  </View> : <View style={styles.systemMessageContainer}>
    <Text style={styles.systemMessageText}>{message.message}</Text>
  </View>;
}

const mainContainer = {
  backgroundColor: '#393e42',
  borderRadius: 10,
  borderBottomLeftRadius: 0,
  paddingLeft: 10,
  paddingTop: 3,
  paddingRight: 10,
  paddingBottom: 3,
  marginTop: 2,
  flexDirection: 'row'
};

const page = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 5
  },
  containerReverse: {
    flexDirection: 'row-reverse',
    marginTop: 5
  },
  avatarContainer: {
    justifyContent: 'flex-end',
    margin: 5
  },
  avatar: {
    borderRadius: 30,
    width: 40,
    height: 40
  },
  nameAndTextContainer: {

  },
  name: {
    fontSize: 17,
    color: '#7779d4',
    fontWeight: '600'
  },
  message: {
    fontSize: 14,
    color: '#d5d5d5'
  },
  mainContainer,
  mainContainerReverse: {
    ...mainContainer,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 0,
  },
  timeContainer: {
    justifyContent: 'flex-end',
    marginLeft: 10
  },
  time: {
    color: '#a3a3a3',
    fontSize: 10
  },
  systemMessageContainer: {
    alignItems: 'center',
    padding: 5
  },
  systemMessageText: {
    color: '#82a7c1',
    fontWeight: '700',
    letterSpacing: 1
  }
});

const styles = StyleSheet.compose(page);

export default Message;