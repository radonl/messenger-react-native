import React, {useEffect, useState} from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Platform,
  AsyncStorage
} from "react-native";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';

import {useSelector} from "react-redux";
import Message from "./Message/Message";
import BackButton from "../../components/Buttons/BackButton/BackButton";
import {Icon} from "react-native-elements";
import SendMessage from "./SendMessage/SendMessage";

const MessagesScreen = ({history}) => {
  const {selectedChat, userWritingMessage, isPrepareToCreateChat, newChatPartner} = useSelector(state => state.chats);
  const {messages} = useSelector(state => state.messages);
  const [scrollView, setScrollView] = useState(null);

  // useEffect(() => {
  //   console.log(messages);
  // }, [messages])

  // useEffect(() => {
  //   AsyncStorage.removeItem("session").then();
  //   AsyncStorage.getItem("user").then();
  // })
  return <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.backButtonContainer}>
          <BackButton handleClick={() => history.push('/')}/>
        </View>
        <View style={styles.avatarContainer}>
          <Image
            style={styles.avatar}
            source={{uri: isPrepareToCreateChat ?  newChatPartner.avatarPath : selectedChat.avatarPath}}
          />
        </View>
        <View style={styles.mainContainer}>
          <Text style={styles.text}>{isPrepareToCreateChat ? newChatPartner.name : selectedChat.name}</Text>
          {!isPrepareToCreateChat && <Text style={styles.membersText}>{selectedChat.members?.length} members</Text>}
        </View>
        <View style={styles.menuIconContainer}>
          <Menu>
            <MenuTrigger>
              <Icon
                color='white'
                name='ellipsis-v'
                type='font-awesome'
              />
            </MenuTrigger>
            <MenuOptions>
              <MenuOption  style={styles.menuOption} onSelect={() => history.push('editGroup')}>
                <Text style={styles.menuOptionText}>Edit</Text>
              </MenuOption>
              <MenuOption style={styles.menuOption}>
                <Text style={styles.menuOptionText}>Leave group</Text>
              </MenuOption>
            </MenuOptions>
          </Menu>
        </View>
      </View>
      <ScrollView
        style={styles.messages}
        ref={ref => setScrollView(ref)}
        onContentSizeChange={() => scrollView.scrollToEnd({animated: true})}
      >
        {selectedChat.id && messages && messages[selectedChat.id] && messages[selectedChat.id]
          .map((message, index) => <Message message={message} key={`${message.id}.${index}`}/>)}
      </ScrollView>
      {userWritingMessage !== '' && <View style={styles.userWritingMessageContainer}>
        <Text style={styles.userWritingMessage}>{userWritingMessage}</Text>
      </View>}
      <SendMessage />
  </View>
};


const page = StyleSheet.create({
  header: {
    backgroundColor: '#393e42',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 0,
    paddingTop: 30,
    width: '100%',
  },
  text: {
    color: 'white',
    fontSize: 20,
    fontWeight: '600',
    letterSpacing: 1.5
  },
  container: {
    backgroundColor: '#292e32',
    height: '100%'
  },
  avatar: {
    borderRadius: 30,
    width: 50,
    height: 50
  },
  backButtonContainer: {
    width: 70
  },
  avatarContainer: {
    width: 80,
    alignItems: 'center'
  },
  membersText: {
    color: '#e5e5e5'
  },
  mainContainer: {
    flexGrow: 1
  },
  menuIconContainer: {
    width: 50
  },
  messages: {
    margin: 10,
    flexGrow: 1,
    ...Platform.select({
      web: {
        height: 0
      }
    })
  },
  userWritingMessageContainer: {
    paddingLeft: 20
  },
  userWritingMessage: {
    color: 'white'
  },
  menuOption: {
    padding: 15,
    backgroundColor: '#494e52'
  },
  menuOptionText: {
    color: 'white',
    fontSize: 16
  }
});

const styles = StyleSheet.compose(page);


export default MessagesScreen;
