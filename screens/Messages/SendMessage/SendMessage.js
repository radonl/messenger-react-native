import {Text, View, TextInput, StyleSheet} from "react-native";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {createChat, sendFile, sendMessage, writingMessage} from "../../../actions/chats/chats.actions";
import {Icon} from "react-native-elements";
import {BaseButton} from "../../../components/Buttons/BaseButton/BaseButton";
import * as ImagePicker from "expo-image-picker";
const SendMessage = () => {
  const {selectedChat, newChatPartner} = useSelector(state => state.chats);
  const dispatch = useDispatch();
  const [message, setMessage] = useState('');
  const messageChanged = text => {
    setMessage(text);
    dispatch(writingMessage(selectedChat.id));
  };
  const pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        base64: true,
        quality: 1,
      });
      if (!result.cancelled) {
        dispatch(sendFile({
          fileContent: result.base64,
          fileExtension: 'jpg',
          selectedChatId: selectedChat.id
        }));
      }
    } catch (e) {
      alert(e);
    }
  };
  useEffect(() => {
    setMessage('');
  }, [selectedChat])
  return <View style={styles.container}>
    <View style={styles.photoContainer}>
      <BaseButton handleClick={pickImage}>
        <View>
          <Icon
            color='white'
            name='photo'
            type='material'
          />
        </View>
      </BaseButton>
    </View>
    <TextInput
      value={message}
      style={styles.message}
      multiline={true}
      onChangeText={text => messageChanged(text)}
    />
    {message !== '' && <View style={styles.sendContainer}>
      <BaseButton handleClick={() => {
        dispatch(
          selectedChat.id ?
            sendMessage({selectedChatId: selectedChat.id, message}):
            createChat({partner: newChatPartner, message})
        );
        setMessage('');
      }}>
        <View>
          <Icon
            color='white'
            name='send'
            type='material'
          />
        </View>
      </BaseButton>
    </View>}
  </View>
};

const page = StyleSheet.create({
  container: {
    minHeight: 40,
    flexDirection: 'row'
  },
  photoContainer: {
    width: 50,
    justifyContent: 'center'
  },
  message: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#494e52',
    margin: 5,
    flexGrow: 1,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 2,
    paddingBottom: 2,
    color: 'white'
  },
  sendContainer: {
    width: 35,
    justifyContent: 'center'
  },
})
const styles = StyleSheet.compose(page);

export default SendMessage;