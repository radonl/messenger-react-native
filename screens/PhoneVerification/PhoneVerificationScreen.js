import {Platform, StyleSheet, Text, TextInput, View} from "react-native";
import React, {useState} from "react";
import {Icon} from "react-native-elements";

import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import {useDispatch, useSelector} from "react-redux";
import {authorization, codeChanged} from "../../actions/login/login.actions";
import BackButton from "../../components/Buttons/BackButton/BackButton";
import NextButton from "../../components/Buttons/NextButton/NextButton";
import Header from "../../components/Header/Header";

const PhoneVerificationScreen = ({history}) => {
  const {code, phone} = useSelector(state => state.login);
  const dispatch = useDispatch();
  const ref = useBlurOnFulfill({value: code, cellCount: 6});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value: code,
    setValue: code => dispatch(codeChanged(code)),
  });
  const auth = () => {
    dispatch(authorization({phone, code, history}));
  }
  return <View style={composedPage.container}>
    <Header title='Phone verification' backUrl='/' history={history} marginLeft={-50}/>
    <View style={composedPage.bodyContainer}>
      <Icon
        color='white'
        name='sms'
        size={60}
        type='material'
      />
      <Text style={composedPage.enterYourCode}>Enter your code</Text>
      <Text style={composedPage.weSentSMS}>We sent an SMS with a verification code</Text>
      <Text style={composedPage.weSentSMSMT}>to your phone +380{phone}</Text>
      <View style={composedPage.codeContainer}>
        <CodeField
          ref={ref}
          {...props}
          value={code}
          onChangeText={code => dispatch(codeChanged(code))}
          cellCount={6}
          rootStyle={composedPage.codeFiledRoot}
          keyboardType="number-pad"
          renderCell={({index, symbol, isFocused}) => (
            <Text
              key={index}
              style={[composedPage.cell, isFocused && composedPage.focusCell]}
              onLayout={getCellOnLayoutHandler(index)}>
              {symbol || (isFocused ? <Cursor /> : null)}
            </Text>
          )}
        />
      </View>
    </View>
    {code.length === 6 && <View style={composedPage.nextContainer}>
      <View style={composedPage.nextSubContainer}>
        <NextButton handleClick={auth}/>
      </View>
    </View>}
  </View>
};

const weSentSMS = {
  marginTop: 20,
  color: '#7b7b7b',
  fontSize: 17
};

const page = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    marginTop: 50,
    alignItems: 'center'
  },
  enterYourCode: {
    marginTop: 10,
    fontSize: 20,
    fontWeight: '600',
    color: 'white'
  },
  weSentSMS,
  weSentSMSMT: {
    ...weSentSMS,
    marginTop: 0
  },
  codeContainer: {
    marginTop: 20,
    flexDirection: 'row'
  },
  codeFiledRoot: {
    marginTop: 20
  },
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    fontSize: 24,
    borderWidth: 1,
    color: 'white',
    borderColor: '#7b7b7b',
    margin: 5,
    textAlign: 'center',
  },
  focusCell: {
    borderColor: 'white',
    borderWidth: 2,
  },
  text: {
    marginLeft: 10
  },
  nextContainer: {
    flex: 1,
    flexDirection: 'column-reverse',
  },
  nextSubContainer: {
    flexDirection: 'row-reverse',
    padding: 20,
  }
});


const composedPage = StyleSheet.compose(page)

export default PhoneVerificationScreen;