import {Text, View, TextInput, ScrollView, Image} from "react-native";
import React, {useEffect, useState} from "react";
import Header from "../../components/Header/Header";
import {BaseButton} from "../../components/Buttons/BaseButton/BaseButton";
import {Icon} from "react-native-elements";
import {useDispatch, useSelector} from "react-redux";
import User from "../../components/User/User";
import NextButton from "../../components/Buttons/NextButton/NextButton";
import {createGroup, prepareToCreateGroup} from "../../actions/chats/chats.actions";
import * as ImagePicker from "expo-image-picker";
import NameAndAvatar from "../../components/Shared/NameAndAvatar/NameAndAvatar";

const SelectNewGroupNameScreen = ({history}) => {
  const {newGroup} = useSelector(state => state.chats);
  const users = useSelector(state => state.users);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [name, setName] = useState('');
  const [avatar, setAvatar] = useState('');
  const dispatch = useDispatch();
  useEffect(() => {
    setSelectedUsers(users.filter(user => newGroup.selectedUsers.includes(user.id)).map(user => {
      user.selected = false;
      return user;
    }));
  }, [users]);
  return <View style={styles.container}>
    <Header title='Create group' backUrl='createGroup' history={history} marginLeft={-50}/>
    <ScrollView>
      <NameAndAvatar
        avatar={avatar}
        avatarChanged={avatar => setAvatar(avatar)}
        name={name}
        nameChanged={name => setName(name)}
      />
      <View style={styles.membersContainer}>
        <Text style={styles.membersCount}>{newGroup.selectedUsers.length} members</Text>
        <ScrollView>
          {!!selectedUsers && selectedUsers.map((user, index) => {
            return <User chat={user} history={history} key={`${user.id}.${index}`}/>
          })}
        </ScrollView>
      </View>
    </ScrollView>
    {name.length > 0 && <NextButton
      handleClick={() => {
        dispatch(createGroup({
          name,
          users: newGroup.selectedUsers,
          ...avatar.length > 0 ? {
            avatar: {
              fileExtension: 'png',
              fileContent: avatar.indexOf('base64') ? avatar.substr(22) : avatar
            }
          } : {}
        }))
        history.push('/');
      }}
      mainContainerStyles={{
        position: 'absolute',
        right: 10,
        bottom: 10,
      }}
    />}
  </View>
};

const styles = {
  container: {
    flex: 1
  },
  membersContainer: {
    backgroundColor: '#303337',
    marginTop: 10,
    padding: 20
  },
  membersCount: {
    color: 'white'
  },
  nextContainer: {}
};

export default SelectNewGroupNameScreen;