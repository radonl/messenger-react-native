import React, {useEffect, useState} from "react";
import {View, Text, StyleSheet, TextInput, ScrollView} from "react-native";
import Header from "../../components/Header/Header";
import {SearchBar} from "react-native-elements";
import {useDispatch, useSelector} from "react-redux";
import User from "../../components/User/User";
import NextButton from "../../components/Buttons/NextButton/NextButton";
import {prepareToCreateGroup} from "../../actions/chats/chats.actions";

const SelectUsersForNewGroupScreen = ({history}) => {
  const [search, setSearch] = useState('');
  const [groupUsers, setGroupUsers] = useState([]);
  const users = useSelector(state => state.users);
  const user = useSelector(state => state.user);
  const {newGroup} = useSelector(state => state.chats);
  const dispatch = useDispatch();
  useEffect(() => {
    setGroupUsers(users.filter(savedUser => savedUser.id !== user.id).map(user => {
      user.selected = false;
      return user;
    }));
  }, [user, users]);
  const selectUser = user => {
    setGroupUsers(groupUsers.map(gUser => {
      if (gUser.id === user.id) {
        gUser.selected = !gUser.selected;
      }
      return gUser;
    }))
  };
  return <View style={styles.container}>
    <Header title='Create group' backUrl='/' history={history} marginLeft={-50}/>
    <SearchBar
      containerStyle={{
        borderBottomColor: 'transparent',
        borderTopColor: 'transparent'
      }}
      placeholder="Search users"
      onChangeText={setSearch}
      value={search}
    />
    <ScrollView>
      {!!groupUsers && groupUsers.map((user, index) => {
        return <User chat={user} history={history} key={`${user.id}.${index}`} handleClick={() => selectUser(user)}/>
      })}
    </ScrollView>
    <View style={styles.nextContainer}>
      <NextButton
        handleClick={() => {
          dispatch(prepareToCreateGroup(groupUsers.filter(user => user.selected).map(user => user.id)));
          history.push('createGroupName');
        }}
        mainContainerStyles={{
          position: 'absolute',
          bottom: 10,
          right: 10
        }}/>
    </View>
  </View>
};

const page = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292e32',
  },
  nextContainer: {
    position: 'relative'
  }
});

const styles = StyleSheet.compose(page);

export default SelectUsersForNewGroupScreen;