import React, {useEffect, useState} from "react";
import {Text, TextInput, View} from "react-native";
import Header from "../../components/Header/Header";
import NameAndAvatar from "../../components/Shared/NameAndAvatar/NameAndAvatar";
import {useDispatch, useSelector} from "react-redux";
import SaveButton from "../../components/Buttons/SaveButton/SaveButton";
import {BaseButton} from "../../components/Buttons/BaseButton/BaseButton";
import {avatarChanged, personalDataFormSubmit} from "../../actions/user/user.actions";
import {logout} from "../../actions/logout/logout.actions";

const SettingsScreen = ({history}) => {
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();
  const [avatar, setAvatar] = useState('');
  const [name, setName] = useState('');
  const [username, setUsername] = useState('');
  useEffect(() => {
    setName(user.displayedName);
    setAvatar(user.avatarPath);
    setUsername(user.username);
  }, [user])
  return <View style={styles.container}>
    <Header history={history} backUrl={'/'} title='Settings' marginLeft={-50}/>
    <NameAndAvatar
      avatar={avatar}
      avatarChanged={avatar => {
        setAvatar(avatar);
      }}
      name={name}
      nameChanged={name => setName(name)}
      namePlaceholder='Enter your name'
    />
    <View style={styles.userNameContainer}>
      <View>
        <Text style={styles.text}>Username</Text>
      </View>
      <View style={styles.userNameInputContainer}>
        <TextInput
          style={styles.userNameInput}
          placeholder='Enter your username'
          onChangeText={text => setUsername(text)}
          defaultValue={username}
        />
      </View>
    </View>
    <BaseButton styles={styles.userNameContainer} handleClick={() => dispatch(logout(history))}>
      <Text style={styles.text}>Logout</Text>
    </BaseButton>
    {(name !== user.displayedName || avatar !== user.avatarPath || username !== user.username ) && <SaveButton
      handleClick={() => {
         if(user.avatarPath !== avatar) {
           dispatch(avatarChanged({
             avatar: {
               fileExtension: 'png',
               fileContent: avatar.indexOf('base64') ? avatar.substr(22) : avatar
             }
           }))
         }
         if(username !== user.username || name !== user.displayedName) {
           dispatch(personalDataFormSubmit({
             displayedName: name,
             username
           }))
         }
      }}
      mainContainerStyles={{
        position: 'absolute',
        right: 10,
        bottom: 10,
      }}
    />}
  </View>;
};

const styles = {
  container: {
    flex: 1
  },
  userNameContainer: {
    margin: 10,
    padding: 20,
    backgroundColor: '#303337'
  },
  text: {
    color: 'white'
  },
  userNameInputContainer: {
    marginTop: 10
  },
  userNameInput: {
    backgroundColor: '#404347',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 3,
    color: 'white'
  }
}

export default SettingsScreen;