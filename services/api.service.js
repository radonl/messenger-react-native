import {API_URL} from 'react-native-dotenv';
import { AsyncStorage } from 'react-native';

class Api {
  async _call({ url, method = "GET", body = null }) {
    const session = JSON.parse(await AsyncStorage.getItem("session"));
    const options = {
      method,
      headers: {
        "Content-Type": "application/json",
        ...session ? { Authorization: session.token } : {}
      }
    };

    if (body) {
      options.body = JSON.stringify(body);
    }

    try {
      const response = await fetch(`${API_URL}/${url}`, options);
      const responseBody = await response.json();

      if (!response.ok) {
        if(response.status === 401) {
          await AsyncStorage.removeItem('session');
          await AsyncStorage.removeItem('user');
          window.location.reload();
        }
        return Promise.reject({
          body: responseBody,
          message: responseBody.message,
          status: response.status
        });
      }

      return responseBody;
    } catch (err) {
      throw new err;
    }
  }

  get(url) {
    return this._call({ url });
  }

  post(url, body) {
    return this._call({ url, method: "POST", body });
  }

  put(url, body) {
    return this._call({ url, method: "PUT", body });
  }

  delete(url, body) {
    return this._call({ url, method: "DELETE", body });
  }
}

export default new Api();
