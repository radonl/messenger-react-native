import Api from './api.service';
import jwt_decode from 'jwt-decode';

import { AsyncStorage } from 'react-native';

class AuthorizationService {
  async sendCode({phone}) {
    try {
      return await Api.post('sendVerificationCode', {
        phone: `380${phone}`
      })
    } catch (e) {
      throw e;
    }
  }
  async login({phone, code, history}) {
    try {
      const result = await Api.post('login', {
        phone: `380${phone}`,
        code
      });
      await AsyncStorage.setItem('session', JSON.stringify(result.data.session));
      await AsyncStorage.setItem('user', JSON.stringify(result.data.user));
      history.push('/');
    } catch (e) {
      throw e;
    }
  }
  async logout(history) {
    try {
      await Api.post('logout');
      await AsyncStorage.removeItem('session');
      await AsyncStorage.removeItem('user');
      history.push('/');
    } catch (e) {
      throw e;
    }
  }

  async isAuthorized() {
    const session = JSON.parse(await AsyncStorage.getItem("session"));
    if(session) {
      const token = session.token;
      if(token) {
        const decoded = jwt_decode(token);
        if (decoded.exp !== undefined) {
          const date = new Date(0);
          date.setUTCSeconds(decoded.exp);
          return (date.valueOf() > new Date().valueOf())
        }
      }
    }
    return false;
  }
}

export default new AuthorizationService();