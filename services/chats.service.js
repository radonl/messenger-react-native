import Api from './api.service';

class ChatsService {
  async loadChats() {
    return await Api.get('chats');
  }

  async loadMessages({id, limit = 10000, offset=0}) {
    return await Api.get(`chats/${id}/messages?limit=${limit}&offset=${offset}`);
  }

  async sendMessage({selectedChatId, message}) {
    return await Api.post(`chats/${selectedChatId}/messages`, {message});
  }

  async sendFile({fileContent,fileExtension, selectedChatId}) {
   try {
     return await Api.post(`chats/${selectedChatId}/messages/file`,{
       fileContent, fileExtension
     })
   } catch (e) {
     throw e;
   }
  }

  async writingMessage(selectedChatId) {
    return await Api.get(`chats/${selectedChatId}/writing-message`);
  }

  async search(searchString) {
    return await Api.get(`users/search/${searchString}`)
  }

  async createChat({partner, message}) {
     const chat = await Api.post('chats', {
       users: [partner.id]
     });
     return {
       name: partner.displayedName || partner.username || partner.phone,
       avatarPath: partner.avatarPath,
       partnerId: partner.id,
       initMessage: message,
       id: chat.data.id
     }
  }

  async createGroup({name, users, avatar}) {
    return await Api.post('chats', {
      name,
      users,
      avatar
    })
  }

  async sendInitMessage({id, initMessage}) {
    await Api.post(`chats/${id}/messages`, {message: initMessage})
    return {id};
  }

  async removeUserFromGroup({chatId, userId}) {
    try {
      await Api.delete(`chats/${chatId}/users/${userId}`);
      return {
        chatId,
        userId
      }
    } catch (e) {
      throw e;
    }
  }

  async addUserToGroup({chatId, userId}) {
    try {
      await Api.post(`chats/${chatId}/users/${userId}`);
      return {
        chatId,
        userId
      }
    } catch (e) {
      throw e;
    }
  }

  async removeGroup(id) {
    try {
      await Api.delete(`chats/${id}`);
      return id;
    } catch (e) {
      throw e;
    }
  }

  async clearGroup(id) {
    try {
      await Api.delete(`chats/${id}/clear`);
      return id;
    } catch (e) {
      throw e;
    }
  }
  
  async updateGroup({id, name, avatar}) {
    return Api.put(`chats/${id}`, {
      avatar,
      name
    });
  }
}

export default new ChatsService();