import Api from './api.service';
import { AsyncStorage } from 'react-native';

class SettingsService {
  async updateUser({displayedName, avatar, username}) {
    return Api.put('users', {
      displayedName,
      avatar,
      username
    });
  }

  async loadUser() {
    return  await AsyncStorage.getItem('user');
  }
}

export default new SettingsService();