import {
  NativeRouter as Router,
  Link,
  Route,
  Redirect,
  Switch,
  withRouter
} from 'react-router-native'

export default {Link, Route, Redirect, Router, Switch, withRouter}
export         {Link, Route, Redirect, Router, Switch, withRouter}